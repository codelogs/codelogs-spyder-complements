/*
 * The MIT License (MIT)
 * Further resources on The MIT License (MIT)
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin

import ec.edu.utpl.datalab.codelogs.maven.plugin.Configuration
import ec.edu.utpl.datalab.codelogs.maven.plugin.ConfigurationParser
import org.codehaus.plexus.logging.Logger
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import static ec.edu.utpl.datalab.codelogs.maven.plugin.ConfigurationParser.ConfigurationProperties.Property
import static ec.edu.utpl.datalab.codelogs.maven.plugin.ConfigurationParser.ConfigurationProperties.Property.IMPLEMENTATION
import static org.assertj.core.api.Assertions.assertThat

class ConfigurationParserTest {

    @InjectMocks
    private ConfigurationParser parser

    @Mock
    Logger logger

    @BeforeMethod
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this)
        System.clearProperty(Property.NOTIFY_WITH.key())
    }

    @Test
    void 'should return default implementation configuration'() throws Exception {

        // Given
        Properties properties = new Properties()

        // When
        Configuration result = parser.get(properties)

        // Then
        assertThat result.getImplementation() isEqualTo 'send-notification'
    }

    @Test
    void 'should return default configuration'() throws Exception {

        Configuration result = parser.get(new Properties());

        assertThat result.isShortDescription() isTrue()
        assertThat result.threshold isEqualTo(-1)
    }

    @Test
    void 'should return configuration'() {

        Properties properties = new Properties();
        properties << [(IMPLEMENTATION.key()):('test')]
        properties << [(Property.SHORT_DESCRIPTION.key()):('false')]
        properties << [(Property.THRESHOLD.key()):('10')]

        Configuration result = parser.get(properties)

        assertThat result.getImplementation() isEqualTo 'test'
        assertThat result.isShortDescription() isFalse()
        assertThat result.threshold isEqualTo(10)
    }

    @Test
    void 'should not override implementation with property when no property is set'() throws Exception {

        def result = parser.get(this.getClass().getResource('/implementation.properties')).notifierProperties

        assertThat result[IMPLEMENTATION.key()] isEqualTo 'growl'
    }

    @Test
    void 'should override implementation with system property'() throws Exception {

        System.setProperty(Property.NOTIFY_WITH.key(), 'override-implementation')

        def result = parser.get(getClass().getResource('/implementation.properties')).notifierProperties

        assertThat result[IMPLEMENTATION.key()] isEqualTo 'override-implementation'
    }

    @Test
    void 'should override implementation with system property when configuration file is not found'() throws Exception {

        System.setProperty(Property.NOTIFY_WITH.key(), 'override-implementation')

        def result = parser.get(new URL('file:///non-existing.properties')).notifierProperties

        assertThat result[IMPLEMENTATION.key()] isEqualTo 'override-implementation'
    }

    @Test
    void 'should overwrite global configuration with user one'() {

        def result = parser.get(
            getClass().getResource('/implementation.properties'),
            getClass().getResource('/implementation-user.properties')
        ).notifierProperties

        assertThat(result[IMPLEMENTATION.key()]).isEqualTo('snarl')
    }

    @Test
    void 'should use configuration passed by system property'() {

        System.setProperty('notifier.anybar.port', '1111')
        System.setProperty('notifier.anybar.host', 'localhost')

        def result = parser.get(getClass().getResource('/anybar.properties')).notifierProperties

        assertThat result['notifier.anybar.port'] isEqualTo '1111'
        assertThat result['notifier.anybar.host'] isEqualTo 'localhost'
    }
}
