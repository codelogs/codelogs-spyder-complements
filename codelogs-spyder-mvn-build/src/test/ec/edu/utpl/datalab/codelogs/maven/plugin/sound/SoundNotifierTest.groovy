/*
 * The MIT License (MIT)
 * Further resources on The MIT License (MIT)
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin.sound
import ec.edu.utpl.datalab.codelogs.maven.plugin.Configuration
import ec.edu.utpl.datalab.codelogs.maven.plugin.ConfigurationParser
import ec.edu.utpl.datalab.codelogs.maven.plugin.sound.SoundNotifier
import groovy.transform.CompileStatic
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import static org.assertj.core.api.Assertions.assertThat
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

@CompileStatic
class SoundNotifierTest {

    private SoundNotifier notifier

    private Configuration configuration

    @BeforeMethod
    void init() throws Exception {
        notifier = new SoundNotifier()

        configuration = new Configuration()

        def parser = mock ConfigurationParser
        when parser.get() thenReturn configuration
        notifier.configuration = parser
    }

    @Test
    void 'should return true when sound is the choosen notifier'() throws Exception {
        configuration.implementation = 'sound'

        assertThat notifier.shouldNotify() isTrue()
    }

    @Test
    void 'should return false when sound is not the choosen notifier'() throws Exception {
        configuration.implementation = 'growl'

        assertThat notifier.shouldNotify() isFalse()
    }
}
