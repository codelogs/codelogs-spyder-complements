/*
 * The MIT License (MIT)
 * Further resources on The MIT License (MIT)
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin

import ec.edu.utpl.datalab.codelogs.maven.plugin.LifeEventSpy
import ec.edu.utpl.datalab.codelogs.maven.plugin.Notifier
import groovy.transform.CompileStatic
import org.apache.maven.execution.DefaultMavenExecutionResult
import org.apache.maven.execution.MavenExecutionResult
import org.codehaus.plexus.logging.Logger
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import static LifeEventSpy.SKIP_NOTIFICATION
import static org.mockito.Matchers.any
import static org.mockito.Mockito.*

@CompileStatic
class NotificationEventSpyChooserTest {

    @InjectMocks
    private LifeEventSpy chooser

    @Mock
    private Notifier notifier
    @Mock
    private Notifier unexpectedNotifier
    @Mock
    private MavenExecutionResult anEvent
    @Mock
    private Logger logger

    @BeforeMethod
    void setUp() throws Exception {
        chooser = new LifeEventSpy()
        MockitoAnnotations.initMocks(this)

        System.setProperty(SKIP_NOTIFICATION, String.valueOf(false))

        when unexpectedNotifier.shouldNotify() thenReturn false

        when notifier.shouldNotify() thenReturn true
        chooser.availableNotifiers = [notifier]
    }

    @Test
    void 'should not notify if event is not a build result'() throws Exception {
        chooser.init({ Collections.emptyMap() })
        chooser.onEvent('this is not a build result')
        chooser.close()

        verify(notifier, never()).onEvent(any(MavenExecutionResult))
    }

    @Test
    void 'should not notify when property skipNotification is true'() throws Exception {
        System.setProperty(SKIP_NOTIFICATION, String.valueOf(true))

        chooser.init({ Collections.emptyMap() })
        chooser.onEvent(anEvent)
        chooser.close()

        verify(notifier, never()).onEvent(any(MavenExecutionResult))
    }

    @Test
    void 'should notify when property skipNotification is false'() throws Exception {
        System.setProperty(SKIP_NOTIFICATION, String.valueOf(false))

        chooser.init({ Collections.emptyMap() })
        chooser.onEvent(anEvent)
        chooser.close()

        verify(notifier).onEvent(anEvent)
    }

    @Test
    void 'should notify failure when build fails without project'() throws Exception {
        DefaultMavenExecutionResult event = new DefaultMavenExecutionResult()
        event.project = null
        event.addException(new NullPointerException())

        chooser.init({ Collections.emptyMap() })
        chooser.onEvent(event)
        chooser.close()

        verify(notifier).onFailWithoutProject(event.getExceptions())
        verify(notifier, never()).onEvent(event)
    }

    @Test
    void 'should send notification with configured notifier'() throws Exception {
        chooser.availableNotifiers = [unexpectedNotifier, notifier]

        chooser.init({ Collections.emptyMap() })
        chooser.onEvent(anEvent)
        chooser.close()

        verify(notifier).onEvent(anEvent)
    }

    @Test
    void 'should not fail when no notifier is configured'() throws Exception {
        chooser.availableNotifiers = [unexpectedNotifier]

        chooser.init({ Collections.emptyMap() })
        chooser.onEvent(anEvent)
        chooser.close()

        verify(unexpectedNotifier, never()).onEvent(any(MavenExecutionResult))
    }

    @Test
    void 'should close notifier'() throws Exception {
        chooser.init({ Collections.emptyMap() })
        chooser.onEvent(anEvent)
        chooser.close()

        verify(notifier).close()
    }
}
