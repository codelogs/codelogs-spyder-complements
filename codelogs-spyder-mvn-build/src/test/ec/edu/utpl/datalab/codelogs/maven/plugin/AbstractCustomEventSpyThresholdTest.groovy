/*
 * The MIT License (MIT)
 * Further resources on The MIT License (MIT)
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin

import com.google.common.base.Stopwatch
import ec.edu.utpl.datalab.codelogs.maven.plugin.AbstractCustomEventSpy
import ec.edu.utpl.datalab.codelogs.maven.plugin.Configuration
import ec.edu.utpl.datalab.codelogs.maven.plugin.ConfigurationParser
import groovy.transform.CompileStatic
import org.apache.maven.execution.DefaultMavenExecutionResult
import org.apache.maven.execution.MavenExecutionResult
import org.codehaus.plexus.logging.Logger
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import static ec.edu.utpl.datalab.codelogs.maven.plugin.ConfigurationParser.ConfigurationProperties.Property.THRESHOLD
import static java.util.concurrent.TimeUnit.SECONDS
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.verifyZeroInteractions
import static org.mockito.Mockito.when

@CompileStatic
class AbstractCustomEventSpyThresholdTest {

    private TestNotifier spy
    private DummyNotifier notifier
    private Configuration configuration

    @BeforeMethod
    void init() throws Exception {
        notifier = mock(DummyNotifier)
        spy = new TestNotifier(notifier: notifier)
        initConfiguration(spy)
        spy.logger = mock(Logger)
    }

    @Test
    void 'should not send notification when build ends before threshold'() throws Exception {
        configuration.threshold = 10
        build_will_last(SECONDS.toNanos(2L))

        spy.init({ Collections.emptyMap() })
        spy.onEvent(new DefaultMavenExecutionResult())

        verifyZeroInteractions(notifier)
    }

    @Test
    void 'should send notification when build ends after threshold'() throws Exception {
        configuration.threshold = 1
        build_will_last(SECONDS.toNanos(2L))

        spy.init({ Collections.emptyMap() })
        spy.onEvent(new DefaultMavenExecutionResult())

        verify(notifier).send()
    }

    @Test
    void 'should send notification when threshold is set to its default value'() throws Exception {
        configuration.threshold = THRESHOLD.defaultValue() as int
        build_will_last(SECONDS.toNanos(2L))

        spy.init({ Collections.emptyMap() })
        spy.onEvent(new DefaultMavenExecutionResult())

        verify(notifier).send()
    }

    private void build_will_last(Long time) {
        def stopwatch = new Stopwatch(new KnownElapsedTimeTicker(time))
        spy.stopwatch = stopwatch
    }

    private void initConfiguration(TestNotifier notifier) {
        configuration = new Configuration()
        def parser = mock(ConfigurationParser)
        when parser.get() thenReturn configuration
        notifier.configuration = parser
    }

    static class TestNotifier extends AbstractCustomEventSpy {
        private DummyNotifier notifier

        @Override
        protected void fireNotification(MavenExecutionResult event) {
            notifier.send()
        }
    }
}
