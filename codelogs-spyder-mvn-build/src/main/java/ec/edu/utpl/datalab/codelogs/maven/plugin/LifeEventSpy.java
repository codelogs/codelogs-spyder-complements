/*
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin;

import com.google.common.annotations.VisibleForTesting;
import ec.edu.utpl.datalab.codelogs.maven.plugin.stadistics.Statistics;
import org.apache.maven.eventspy.AbstractEventSpy;
import org.apache.maven.eventspy.EventSpy;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.ExecutionEvent;
import org.apache.maven.execution.MavenExecutionResult;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.logging.Logger;
import org.eclipse.aether.RepositoryEvent;

import java.util.LinkedHashSet;
import java.util.List;

import static org.apache.maven.execution.ExecutionEvent.Type.SessionEnded;
import static org.apache.maven.execution.ExecutionEvent.Type.SessionStarted;

@Component(role = EventSpy.class, hint = "notification", description = "Send notification to indicate build status.")
public class LifeEventSpy extends AbstractEventSpy {

    public static final String SKIP_NOTIFICATION = "skipNotification";

    @Requirement
    private Logger logger;

    @Requirement
    private List<Notifier> availableNotifiers;

    private Notifier activeNotifier;

    private Statistics statistics = new Statistics();

    @Override
    public void init(Context context) throws Exception {
        chooseNotifier();
        activeNotifier.init(context);
    }

    @Override
    public void onEvent(Object event) throws Exception {
        if (shouldSendNotification()) {
            if (isExecutionResult(event) && hasFailedWithoutProject((MavenExecutionResult) event)) {
                activeNotifier.onFailWithoutProject(((MavenExecutionResult) event).getExceptions());
            } else if (isExecutionResult(event)) {
                activeNotifier.onEvent((MavenExecutionResult) event);
            }
        }
    }

    private boolean hasFailedWithoutProject(MavenExecutionResult event) {
        return event.getProject() == null && event.hasExceptions();
    }

    private boolean shouldSendNotification() {
        return !"true".equalsIgnoreCase(System.getProperty(SKIP_NOTIFICATION));
    }

    @Override
    public void close() throws Exception {
        activeNotifier.close();
    }

    private boolean isExecutionResult(Object event) {
        return event instanceof MavenExecutionResult;
    }

    private void chooseNotifier() {
        for (Notifier notifier : availableNotifiers) {
            if (notifier.shouldNotify()) {
                activeNotifier = notifier;
                logger.debug("Will notify build success/failure with: " + activeNotifier);
                return;
            }
        }

        if (activeNotifier == null) {
            activeNotifier = UselessNotifier.EMPTY;
        }
    }

    @VisibleForTesting
    void setAvailableNotifiers(List<Notifier> availableNotifiers) {
        this.availableNotifiers = availableNotifiers;
    }

    /********** Profile Event ***************/
    private void profilerEvent(Object event){
            if (event instanceof DefaultMavenExecutionRequest) {
                DefaultMavenExecutionRequest mavenEvent = (DefaultMavenExecutionRequest) event;
                statistics.setGoals(new LinkedHashSet<String>(mavenEvent.getGoals()));
                statistics.setProperties(mavenEvent.getUserProperties());
            } else if (event instanceof ExecutionEvent) {
                storeExecutionEvent((ExecutionEvent) event);
                trySaveTopProject((ExecutionEvent) event);
            } else if (event instanceof RepositoryEvent) {
                storeDownloadingArtifacts((RepositoryEvent) event);
            }
    }
    private void trySaveTopProject(ExecutionEvent event) {

        if(event.getType() == SessionEnded){
            System.out.println("== END codelogs complement == "  + event.getSession().getCurrentProject());
        }
        if (event.getType() == SessionStarted) {
            System.out.println("== Start codelogs complement == ");
            statistics.setTopProject(event.getProject());
        }
    }

    private void storeDownloadingArtifacts(RepositoryEvent event) {
        logger.debug(String.format("Received event (%s): %s", event.getClass(), event));
        switch (event.getType()) {
            case ARTIFACT_DOWNLOADING:
                statistics.startDownload(event.getArtifact());
                break;
            case ARTIFACT_DOWNLOADED:
                if (hasNoException(event)) {
                    statistics.stopDownload(event.getArtifact());
                }
                break;
        }
    }
    private static boolean hasNoException(RepositoryEvent event) {
        List<Exception> exceptions = event.getExceptions();
        return exceptions == null || exceptions.isEmpty();
    }

    private void storeExecutionEvent(ExecutionEvent event) {
        logger.debug(String.format("Received event (%s): %s", event.getClass(), event));

        MavenProject currentProject = event.getSession().getCurrentProject();
        switch (event.getType()) {
            case ProjectStarted:
                statistics.startProject(currentProject);
                break;
            case ProjectSucceeded:
            case ProjectFailed:
                statistics.stopProject(currentProject);
                break;
            case MojoStarted:
                statistics.startExecution(currentProject, event.getMojoExecution());
                break;
            case MojoSucceeded:
            case MojoFailed:
                statistics.stopExecution(currentProject, event.getMojoExecution());
                break;
        }
    }
}
