package ec.edu.utpl.datalab.codelogs.maven.plugin;

import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatBuildRunPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.TYPE_INSTRUCTION_PACK;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.VFS;
import org.apache.maven.execution.MavenExecutionResult;

import java.util.List;

/**
 * * Created by rfcardenas
 */
public class MavenResultAdapter implements ResultAdapter {

    public HeartBeatBuildRunPack adapter(MavenExecutionResult e) {
        HeartBeatBuildRunPack compileRunPack = new HeartBeatBuildRunPack();
        String pathProject = (e.getProject()!=null)?e.getProject().getBasedir().getPath():"";


        List<Throwable> problems = e.getExceptions();
        System.out.println("Problemas " + problems.size());
        if(!problems.isEmpty()){

            Throwable ex = problems.get(0);
            compileRunPack.setMessageL1(ex.getMessage());
            compileRunPack.setMessageL2(ex.toString());
            compileRunPack.setMessageL3(ex.getCause().toString());
            compileRunPack.setInstruction(TYPE_INSTRUCTION_PACK.HEART_BEAT_COMPILE_FAILED);

        }else{
            compileRunPack.setMessageL1("Path project " + pathProject);
            compileRunPack.setMessageL2("Total dependencias " + e.getDependencyResolutionResult().getDependencies().size());
            compileRunPack.setInstruction(TYPE_INSTRUCTION_PACK.HEART_BEAT_COMPILE_SUCCESS);
        }
        compileRunPack.setTime(e.getBuildSummary(e.getProject()).getTime());


        try {
            FileObject universalPath = VFS.getManager().resolveFile(pathProject);
            compileRunPack.setPathContext(universalPath.getName().getFriendlyURI());
        } catch (FileSystemException e1) {
            e1.printStackTrace();
        }
        return compileRunPack;
    }

}
