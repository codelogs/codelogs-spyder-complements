/*
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin.sound;

import ec.edu.utpl.datalab.codelogs.maven.plugin.AbstractCustomEventSpy;
import ec.edu.utpl.datalab.codelogs.maven.plugin.Notifier;
import ec.edu.utpl.datalab.codelogs.maven.plugin.Status;
import org.apache.maven.execution.MavenExecutionResult;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.util.IOUtil;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component(role = Notifier.class, hint = "sound")
public class SoundNotifier extends AbstractCustomEventSpy {

    @Override
    public void onFailWithoutProject(List<Throwable> exceptions) {
        playSound(Status.FAILURE);
    }

    @Override
    protected void fireNotification(MavenExecutionResult event) {
        playSound(getBuildStatus(event));
    }

    private void playSound(Status status) {
        AudioInputStream ais = getAudioStream(status);
        if (ais == null) {
            logger.warn("Cannot get a sound to play. Skipping notification...");
            return;
        }
        play(ais);
    }

    private void play(AudioInputStream ais) {
        try {
            Clip clip = AudioSystem.getClip();
            EndListener listener = new EndListener();
            clip.addLineListener(listener);
            clip.open(ais);
            playAndWait(clip, listener);
        } catch (LineUnavailableException e) {
            fail(e);
        } catch (IOException e) {
            fail(e);
        } finally {
            IOUtil.close(ais);
        }
    }

    private void playAndWait(Clip clip, EndListener listener) {
        try {
            clip.start();
            listener.waitEnd();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            clip.close();
        }
    }

    private void fail(Exception e) {
        logger.debug("Error playing sound.", e);
    }

    private AudioInputStream getAudioStream(Status success) {
        try {
            return AudioSystem.getAudioInputStream(getUrl(success));
        } catch (UnsupportedAudioFileException e) {
            return noAudioStream(e);
        } catch (IOException e) {
            return noAudioStream(e);
        }
    }

    private AudioInputStream noAudioStream(Exception e) {
        logger.warn("Error reading audio stream.", e);
        return null;
    }

    private InputStream getUrl(Status status) {
        String sound = status == Status.SUCCESS ? "/109662__grunz__success.wav" : "/Sad_Trombone-Joe_Lamb-665429450.wav";
        return getClass().getResourceAsStream(sound);
    }
}
