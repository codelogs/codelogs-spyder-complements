package ec.edu.utpl.datalab.codelogs.maven.plugin;

import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatBuildRunPack;
import org.apache.maven.execution.MavenExecutionResult;

/**
 * * Created by rfcardenas
 */
public interface ResultAdapter {
    HeartBeatBuildRunPack adapter(MavenExecutionResult e);
}
