/*
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin.stadistics;

import com.google.common.base.Stopwatch;
import org.eclipse.aether.artifact.Artifact;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static com.google.common.base.Objects.firstNonNull;

public class Data {

    private List<Project> projects;
    private List<EntryAndTime<Artifact>> downloads;
    private Stopwatch totalDownloadTime;
    private Stopwatch buildTime;
    private Date date;
    private String name;
    private String goals;
    private Properties parameters;

    public List<Project> getProjects() {
        return firstNonNull(projects, Collections.<Project>emptyList());
    }

    public List<EntryAndTime<Artifact>> getDownloads() {
        return firstNonNull(downloads, Collections.<EntryAndTime<Artifact>>emptyList());
    }

    public Stopwatch getTotalDownloadTime() {
        return totalDownloadTime;
    }

    public boolean isDownloadSectionDisplayed() {
        return downloads != null && !downloads.isEmpty();
    }

    public Stopwatch getBuildTime() {
        return buildTime;
    }

    public Date getDate() {
        return date;
    }

    public String getFormattedDate() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date);
    }

    public String getName() {
        return name;
    }

    public String getGoals() {
        return goals;
    }

    public Properties getParameters() {
        return parameters == null ? new Properties() : parameters;
    }

    public Data setProjects(List<Project> projects) {
        this.projects = projects;
        return this;
    }

    public Data setDownloads(List<EntryAndTime<Artifact>> downloads) {
        this.downloads = downloads;
        return this;
    }

    public Data setTotalDownloadTime(Stopwatch time) {
        this.totalDownloadTime = time;
        return this;
    }

    public Data setBuildTime(Stopwatch time) {
        this.buildTime = time;
        return this;
    }

    public Data setDate(Date date) {
        this.date = date;
        return this;
    }

    public Data setName(String name) {
        this.name = name;
        return this;
    }

    public Data setGoals(String goals) {
        this.goals = goals;
        return this;
    }

    public Data setParameters(Properties parameters) {
        this.parameters = parameters;
        return this;
    }
}
