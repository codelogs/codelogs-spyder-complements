#Maven Codelogs

Esta es una extesión para capturar datos sobre el comportamiento y problemas 
en la compilacion de un programa, este tipo de datos son utiles 

La recolección de estos datos se basa en la aplicabilidad en el campo de Learning Analytic,
el algoritmo propuesto por (Watson, Li, & Godwin) , trabaja con datos de errores de
compilación, con el fin de predecir el rendimiento de los estudiantes.
A partir del trabajo de (Matthew C.) sobre el algorimo "Error Quotient", se han propuesto algorimos similares,
sirviendo de base para algoritmos y variantes con el mismo enfoque, pese a que
en su momento no pudo aseverar la credibilidad de su algoritmo debido a la falta de datos,
Recientemente en el año 2015 (Matthew C & Dorn B) pusieron a prueba este con un conjunto
de datos, resultado de las compilaciones de 27,698 usuarios, en este nuevo estudio los
autores manifiesta que este algoritmo puede servir como un sustituto a las medidas
tradicionales de rendimiento.

##Installation

`$M2_HOME` refers to maven installation folder.

```
.
├── bin
├── boot
├── conf
└── lib
``` 

### Requiere Maven >= 3.3.x

Get jar and copy it in `%M2_HOME%/lib/ext`  folder.

*or*
Desde la version 3.3.x de maven, se puede utilizar las extensiones creando un 
directorio .mvn en el proyecto con el siguiente xml

 [core extensions configuration mechanism](http://takari.io/2015/03/19/core-extensions.html) by creating a `${maven.multiModuleProjectDirectory}/.mvn/extensions.xml` file with:

```xml
	<?xml version="1.0" encoding="UTF-8"?>
	<extensions>
	    <extension>
	      <groupId>ec.edu.utpl.datalab.codelogs</groupId>
            <artifactId>codelogs-maven-plugin</artifactId>
            <version>1.0.0-SNAPSHOT</version>
	    </extension>
	</extensions>
```


## Importante

Puesto que se esta trabajando con repositorios basados en git es importante configurar los
siguiente en tu pom.xml

```
<repositories>
        <repository>
            <id>codelogs-maven-repo-snapshots</id>
            <name>Repositorio codelogs versiones estables</name>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
            <url>https://gitlab.com/codelogs/openpack-mvn/raw/snapshots</url>
        </repository>
    </repositories>
```

Y para descargar la extension y tenerla en tu repositorio local, agregalo como una dependencia cualquiera
solo si es que no la tienes ya instalada,

```
 <dependencies>
        <dependency>
            <groupId>ec.edu.utpl.datalab.codelogs</groupId>
            <artifactId>codelogs-maven-plugin</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>
    </dependencies>
```


