/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 * Class demo
 * @author rfcardenas
 */
public class Test2 {
    static String s;
    public static void main(String[] args){
        System.out.println("Mi primera linea de código");
        System.out.println("Mi primera linea de código");
        System.out.println("Mi primera linea de código");
        System.out.println("Mi primera linea de código");
        System.out.println("");
    }
}
/**
 * 
 * Clase para representar una persona
 * 
 * */
class Person{
    private String name;
    /**
     * El nombre de la persona
     * @param name 
     */
    public Person(String name) {
        this.name = name;
    }
    
    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * print data
     * @return 
     */
    @Override
    public String toString() {
        return "Person{" + "name=" + name + '}';
    }
    
}