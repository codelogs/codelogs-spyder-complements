# Agente java

Este es un agente que modifica el bytecode con la finalidad de recolectar
los problemas comunes al momento de aprender programación, se recolecta 
Exceptions de tipo Runtime.
Estos datos son utiles en algormitmos descritos en la documentación del TT.

Para poder monitorear los problemas que se presentan a quienes inician en la programación
se desarrollo un agenta java que modifica el bytecode para poder captar problemas (Exceptions)
a nivel de ejecución de un programa.

Se puede añadir estos a un IDE de forma sencilla, por ejemplo modificando el
comportamiento del boton RUN y añadiendo el siguiente argumento a la clase MAIN

```cmd
-javaagent:..algunpath...m2/repository/ec/edu/utpl/datalab/codelogs/codelogs-hack-bytecode/1.0.0-SNAPSHOT/codelogs-hack-bytecode-1.0.0-SNAPSHOT.jar
```

opciones: 
-port=8484 // este es requerido para especificar el puerto del monitor, aunque por defecto es el 8484
-pathfile="x" // este es usado por transformador, no es necesario configurarlo

Este proceso se automatiza utilizando maven, el cual automaticamente cuando se compila el proyecto, modifica las clases
para que interactuen con el monitor.
