package ec.edu.utpl.datalab.codelogs.hack.bytecode;

import org.junit.Ignore;

/**
 * * Created by rfcardenas
 */
@Ignore
public class Dm2 {
    public static void codeLogsProgramMethod_1_0_0() {
    }

    public static void executeHandleException_1_0_0(){
        try {
            codeLogsProgramMethod_1_0_0();
        } catch (Exception var1) {
            System.out.println("Error " + var1.getMessage());
        }
    }

    public Dm2() {
    }

    public static void main(String[] args) {
        System.out.println("          --------- CODELOGS UTPL ----------");
        System.out.println("UTPL Plugin V1.0");
        System.out.println("==========================================================");
        executeHandleException_1_0_0();
    }
}
