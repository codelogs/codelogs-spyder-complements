package ec.edu.utpl.datalab.codelogs.hack.bytecode;

import java.io.InputStream;

/**
 * Interface de transforma de codigo, puede extir una variedad de trasformadores
 * * Created by rfcardenas
 */
public interface ITransformer {
    ResultTransform transform(InputStream inputStream) throws  Exception;
    ResultTransform transform(String name) throws  Exception;
}
