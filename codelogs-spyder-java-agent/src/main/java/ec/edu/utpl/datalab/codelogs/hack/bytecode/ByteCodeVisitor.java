package ec.edu.utpl.datalab.codelogs.hack.bytecode;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * * Created by rfcardenas
 */
public class ByteCodeVisitor extends ClassVisitor
{
    public ByteCodeVisitor(int api) {
        super(api);
    }


    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        boolean isPublic = (access & Opcodes.ACC_PUBLIC)!=0;
        boolean isStatic = (access & Opcodes.ACC_STATIC)!=0;
        boolean isMain  = name.equals("main");
        boolean isArgs = desc.equals("([Ljava/lang/String;)V");
        if(isPublic && isStatic && isMain && isArgs){
            System.out.println("Encontrado metodo main");

        }
        return super.visitMethod(access, name, desc, signature, exceptions);
    }
}
