package ec.edu.utpl.datalab.codelogs.hack.bytecode;

/**
 * * Created by rfcardenas
 */
public class ResultTransform {
    boolean isOriginal;
    byte[] result;

    public ResultTransform(boolean isOriginal, byte[] result) {
        this.isOriginal = isOriginal;
        this.result = result;
    }

    public boolean isOriginal() {
        return isOriginal;
    }

    public byte[] getResult() {
        return result;
    }
}
