package ec.edu.utpl.datalab.codelogs.hack.bytecode;

/**
 * INTERFAZ TODAS LAS PROPIEDADES SON publicas estaticas y finales
 * * Created by rfcardenas
 */
public interface PROPERTIES {
    String PATHFILE = "pathfile";
    String PORT = "port";
}
