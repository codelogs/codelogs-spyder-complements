package ec.edu.utpl.datalab.codelogs.hack.bytecode;

/**
 * * Created by rfcardenas
 */
public class CustomLoader extends ClassLoader {
    public Class<?> defineClass(String name, byte[] b) {
        return defineClass(name, b, 0, b.length);
    }
    public static Class<?> createClass(byte[] b) {
        return new CustomLoader().defineClass("ec.edu.utpl", b);
    }
}
