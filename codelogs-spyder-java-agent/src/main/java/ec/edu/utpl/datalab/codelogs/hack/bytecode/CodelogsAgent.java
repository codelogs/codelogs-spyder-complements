package ec.edu.utpl.datalab.codelogs.hack.bytecode;


import ec.edu.utpl.datalab.codelogs.spyder.core.env.ArgsEnvironmentProperties;
import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;

import java.lang.instrument.Instrumentation;

/**
 * Agent modificar codigo del programador
 * * Created by rfcardenas
 */
public class CodelogsAgent {
    public static void premain(String agentArgument, Instrumentation instrumentation) {
        System.out.println(agentArgument);
        String[] args = new String[]{
                PROPERTIES.PATHFILE + "=" + "UNK",
                PROPERTIES.PORT + "=" + 8484
        };
        for (String arg : args) {
            System.out.println(arg);
        }
        if(agentArgument!=null)
            args = agentArgument.split(",");
        EnvironmentProperties environmentProperties = new ArgsEnvironmentProperties(args);
        instrumentation.addTransformer(new AgentTransformer(environmentProperties));
    }
}
