package ec.edu.utpl.datalab.codelogs.hack.bytecode;


import ec.edu.utpl.datalab.codelogs.spyder.core.env.EnvironmentProperties;

import java.io.ByteArrayInputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

/**
 * Agente para tranformar el byte code
 * * Created by rfcardenas
 */
public class AgentTransformer implements ClassFileTransformer {

    private static final String[] DEFAULT_EXCLUDES = new String[] {"com/sun/", "sun/", "java/", "javax/", "org/slf4j","com/intellij" };
    private static final String[] DEFAULT_ALLOW = new String[] {"ec/edu" };
    private ITransformer iTransformer;

    public AgentTransformer(EnvironmentProperties properties) {

        String pathFile = properties.getProperty(PROPERTIES.PATHFILE,"UNKNOW");
        int port = properties.getAsInt(PROPERTIES.PORT,8484);
        System.out.println(String.format("Fichero %s " ,pathFile));
        iTransformer =  new Transformer(pathFile,port);
    }

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        for( int i = 0; i < DEFAULT_ALLOW.length; i++ ) {
            if(!className.startsWith( DEFAULT_ALLOW[i] ) ) {
                return classfileBuffer;
            }
        }
        for( int i = 0; i < DEFAULT_EXCLUDES.length; i++ ) {
            if( className.startsWith( DEFAULT_EXCLUDES[i] ) ) {
                return classfileBuffer;
            }
        }
        System.out.println("Mofificando " + className);
        try {
            ResultTransform aresult  = iTransformer.transform(new ByteArrayInputStream(classfileBuffer));
            return aresult.getResult();
        } catch (Exception e) {

        }
        return classfileBuffer;
    }
}
