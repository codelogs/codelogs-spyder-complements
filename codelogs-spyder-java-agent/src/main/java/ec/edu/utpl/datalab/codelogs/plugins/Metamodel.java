package ec.edu.utpl.datalab.codelogs.plugins;


import ec.edu.utpl.datalab.codelogs.spyder.core.os.AbstractConnectionProcess;
import ec.edu.utpl.datalab.codelogs.spyder.core.os.ws.SimpleConnectionSocket;
import ec.edu.utpl.datalab.codelogs.spyder.core.util.JsonMapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.terminal.Command;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatBuildRunPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.TYPE_INSTRUCTION_PACK;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static ec.edu.utpl.datalab.codelogs.spyder.service.comandos.MonitorCommand.CMD_RUN_BEHAVIOR;

/**
 * Modelo para el Hack
 * * Created by rfcardenas
 */
public class Metamodel {
    private static AbstractConnectionProcess connectionProces = new SimpleConnectionSocket(8484,5,200, TimeUnit.MILLISECONDS);
    private static final String pathFile ="RUTA_ARCHIVO_UNKNOW";
    static {
        connectionProces.connect();
    }

    public static void codeLogsProgramMethod_1_0_0() {
    }

    public static void executeHandleException_1_0_0() {
        HeartBeatBuildRunPack blockCompileRun = new HeartBeatBuildRunPack();
        blockCompileRun.setInstruction(TYPE_INSTRUCTION_PACK.HEART_BEAT_RUN_SUCCESS);
        blockCompileRun.setPathContext(pathFile);
        long start = 0, end = 0 , delta = 0;
        try {
            start = System.currentTimeMillis();
            codeLogsProgramMethod_1_0_0();
        } catch (Exception ex) {
            String classname = ex.getClass().getName();
            String message = ex.getMessage();
            String lmessage = (ex.getCause()!=null) ? ex.getCause().getLocalizedMessage():"unknow";
            blockCompileRun.setInstruction(TYPE_INSTRUCTION_PACK.HEART_BEAT_RUN_FAILED);
            blockCompileRun.setMessageL1(message);
            blockCompileRun.setMessageL2(lmessage);
            blockCompileRun.setMessageL3(classname);
            throw new RuntimeException(ex);
        }finally {
            end = System.currentTimeMillis();
            delta = end -start;
            blockCompileRun.setTime(delta);
            try {
                String data = JsonMapper.writte(blockCompileRun);
                Command command = Command.createCommand(CMD_RUN_BEHAVIOR,data).build();
                connectionProces.send(JsonMapper.writte(command));
            } catch (IOException e) {
                e.printStackTrace();
            }
            connectionProces.disconnect();
            System.out.println(String.format("Runtime segundos " + TimeUnit.MILLISECONDS.toSeconds(delta)));
        }
    }
    public static void main(String[] args) {
        System.out.println("          --------- CODELOGS UTPL ----------");
        System.out.println("UTPL Plugin V1.0");
        System.out.println("==========================================================");
        executeHandleException_1_0_0();
    }

}
