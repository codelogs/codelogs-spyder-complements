package ec.edu.utpl.datalab.codelogs.hack.bytecode;


import org.objectweb.asm.*;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodNode;

import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import static org.objectweb.asm.Opcodes.*;

/**
 * Manipulacion del bytecode del programa del estudiante
 * <p>
 * Clase pequeño hack del codigo del estudiante, para capturar los problemas y otros
 * datos del programa del estudiante
 * * Created by rfcardenas
 */
public class Transformer implements ITransformer {
    private static String CLASS_NAME = "";
    private static String PROGRAMM_METHOD = "codeLogsProgramMethod_1_0_0";
    private static String PROGRAMM_WITH_EXC = "executeHandleException_1_0_0";

    private static String pathFile;
    private static int port;
    private static long timeoutattem;
    private static TimeUnit timeUnit;
    private static int attempsNum;

    public Transformer(String path) {
        Transformer.pathFile = path;
        Transformer.port = 8484;
        Transformer.timeoutattem = 200;
        Transformer.timeUnit = TimeUnit.MILLISECONDS;
        Transformer.attempsNum  = 5;
    }

    public Transformer(String path, int port) {
        this(path);
        Transformer.pathFile = path;
        Transformer.port = port;
    }

    public Transformer(String pathFile, int port, long timeoutattem, TimeUnit timeUnit, int attempsNum) {
        Transformer.pathFile = pathFile;
        Transformer.port = port;
        Transformer.timeoutattem = timeoutattem;
        Transformer.timeUnit = timeUnit;
        Transformer.attempsNum = attempsNum;
    }

    /**
     * Transforma el bytecode segun las necesidades
     *
     * @param inputStream
     * @return
     * @throws Exception
     */

    public ResultTransform transform(InputStream inputStream) throws Exception {
        ClassReader classReader = new ClassReader(inputStream);
        return transformClass(classReader);
    }

    @Override
    public ResultTransform transform(String name) throws Exception {
        ClassReader classReader = new ClassReader(name);
        return transformClass(classReader);
    }

    public static ResultTransform transformClass(ClassReader classReader){
        boolean original = true;
        ClassNode classNode = new ClassNode();
        classReader.accept(classNode, ClassReader.SKIP_DEBUG);
        ClassWriter cw = new ClassWriter(classReader, 0);

        CLASS_NAME = classReader.getClassName();
        boolean hasProgrammMethod = classNode.methods.stream()
                .filter(o -> {
                    if (!(o instanceof MethodNode))
                        return false;
                    return ((MethodNode) o).name.equals(PROGRAMM_METHOD);
                }).count() > 0;
        boolean hasProgrammHandlerExc = classNode.methods.stream()
                .filter(o -> {
                    if (!(o instanceof MethodNode))
                        return false;
                    return ((MethodNode) o).name.equals(PROGRAMM_WITH_EXC);
                }).count() > 0;


        if (!hasProgrammHandlerExc && !hasProgrammHandlerExc) {
            for (Object entry : classNode.methods) {
                MethodNode method = (MethodNode) entry;
                String name = (method.name);
                String desc = (method.desc);
                int access = method.access;

                boolean isPublic = (access & Opcodes.ACC_PUBLIC) != 0;
                boolean isStatic = (access & Opcodes.ACC_STATIC) != 0;
                boolean isMain = name.equals("main");
                boolean isArgs = desc.equals("([Ljava/lang/String;)V");
                if (isPublic && isStatic && isMain && isArgs) {
                    InsnList instrucciones = method.instructions;
                    InsnList instruccionesBackup = new InsnList();
                    instruccionesBackup.add(instrucciones);
                    instrucciones.clear();
                    createFields(classNode);
                    createAutoInit(cw);
                    createWrapperProgramm(cw, instruccionesBackup);
                    createWrapperWithException(cw);
                    modificarMain(method);
                }
            }
            original = false;
        }

        classNode.accept(cw);
        return new ResultTransform(original,cw.toByteArray());
    }
    public static void createFields(ClassNode classNode){
        classNode.visitField(ACC_PRIVATE + ACC_FINAL + ACC_STATIC, "pathFile", "Ljava/lang/String;", null, pathFile);
        classNode.visitField(ACC_PRIVATE + ACC_STATIC, "connectionProces", "Lec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess;", null, null);
        classNode.visitEnd();
    }
    public static void createAutoInit(ClassWriter cw){
        MethodVisitor  mv = cw.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
        mv.visitCode();
        Label l0 = new Label();
        mv.visitLabel(l0);
        mv.visitLineNumber(21, l0);
        mv.visitTypeInsn(NEW, "ec/edu/utpl/datalab/codelogs/spyder/core/os/ws/SimpleConnectionSocket");
        mv.visitInsn(DUP);
        mv.visitIntInsn(SIPUSH, 8484);
        mv.visitInsn(ICONST_5);
        mv.visitIntInsn(SIPUSH, 200);
        mv.visitFieldInsn(GETSTATIC, "java/util/concurrent/TimeUnit", "MILLISECONDS", "Ljava/util/concurrent/TimeUnit;");
        mv.visitMethodInsn(INVOKESPECIAL, "ec/edu/utpl/datalab/codelogs/spyder/core/os/ws/SimpleConnectionSocket", "<init>", "(IIILjava/util/concurrent/TimeUnit;)V", false);
        mv.visitFieldInsn(PUTSTATIC, CLASS_NAME, "connectionProces", "Lec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess;");
        Label l1 = new Label();
        mv.visitLabel(l1);
        mv.visitLineNumber(24, l1);
        mv.visitFieldInsn(GETSTATIC, CLASS_NAME, "connectionProces", "Lec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess;");
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess", "connect", "()V", false);
        Label l2 = new Label();
        mv.visitLabel(l2);
        mv.visitLineNumber(25, l2);
        mv.visitInsn(RETURN);
        mv.visitMaxs(6, 0);
        mv.visitEnd();
    }
    /**
     * Crea el metodo wrapper
     *
     * @param cw
     * @param instruccionesBackup
     */
    public static void createWrapperProgramm(ClassWriter cw, InsnList instruccionesBackup) {
        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "codeLogsProgramMethod_1_0_0", "()V", null, null);
        mv.visitCode();
        instruccionesBackup.accept(mv);
        mv.visitInsn(RETURN);
        mv.visitMaxs(14, 4);
        mv.visitEnd();
    }

    /**
     * Crea el metodo ejecutor con exceptions
     *
     * @param cw
     */
    public static void createWrapperWithException(ClassWriter cw) {
        MethodVisitor mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "executeHandleException_1_0_0", "()V", null, null);
        mv.visitCode();
        Label l0 = new Label();
        Label l1 = new Label();
        Label l2 = new Label();
        mv.visitTryCatchBlock(l0, l1, l2, "java/io/IOException");
        Label l3 = new Label();
        Label l4 = new Label();
        Label l5 = new Label();
        mv.visitTryCatchBlock(l3, l4, l5, "java/lang/Exception");
        Label l6 = new Label();
        mv.visitTryCatchBlock(l3, l4, l6, null);
        Label l7 = new Label();
        Label l8 = new Label();
        Label l9 = new Label();
        mv.visitTryCatchBlock(l7, l8, l9, "java/io/IOException");
        Label l10 = new Label();
        mv.visitTryCatchBlock(l5, l10, l6, null);
        Label l11 = new Label();
        mv.visitLabel(l11);
        mv.visitLineNumber(31, l11);
        mv.visitTypeInsn(NEW, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack");
        mv.visitInsn(DUP);
        mv.visitMethodInsn(INVOKESPECIAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "<init>", "()V", false);
        mv.visitVarInsn(ASTORE, 0);
        Label l12 = new Label();
        mv.visitLabel(l12);
        mv.visitLineNumber(32, l12);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitFieldInsn(GETSTATIC, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/TYPE_INSTRUCTION_PACK", "HEART_BEAT_RUN_SUCCESS", "Lec/edu/utpl/datalab/codelogs/spyder/libs/transfers/TYPE_INSTRUCTION_PACK;");
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "setInstruction", "(Lec/edu/utpl/datalab/codelogs/spyder/libs/transfers/TYPE_INSTRUCTION_PACK;)V", false);
        Label l13 = new Label();
        mv.visitLabel(l13);
        mv.visitLineNumber(33, l13);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitLdcInsn(pathFile);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "setPathContext", "(Ljava/lang/String;)V", false);
        Label l14 = new Label();
        mv.visitLabel(l14);
        mv.visitLineNumber(34, l14);
        mv.visitInsn(LCONST_0);
        mv.visitVarInsn(LSTORE, 1);
        Label l15 = new Label();
        mv.visitLabel(l15);
        mv.visitInsn(LCONST_0);
        mv.visitVarInsn(LSTORE, 3);
        Label l16 = new Label();
        mv.visitLabel(l16);
        mv.visitInsn(LCONST_0);
        mv.visitVarInsn(LSTORE, 5);
        mv.visitLabel(l3);
        mv.visitLineNumber(36, l3);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
        mv.visitVarInsn(LSTORE, 1);
        Label l17 = new Label();
        mv.visitLabel(l17);
        mv.visitLineNumber(37, l17);
        mv.visitMethodInsn(INVOKESTATIC, CLASS_NAME, "codeLogsProgramMethod_1_0_0", "()V", false);
        mv.visitLabel(l4);
        mv.visitLineNumber(48, l4);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
        mv.visitVarInsn(LSTORE, 3);
        Label l18 = new Label();
        mv.visitLabel(l18);
        mv.visitLineNumber(49, l18);
        mv.visitVarInsn(LLOAD, 3);
        mv.visitVarInsn(LLOAD, 1);
        mv.visitInsn(LSUB);
        mv.visitVarInsn(LSTORE, 5);
        Label l19 = new Label();
        mv.visitLabel(l19);
        mv.visitLineNumber(50, l19);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitVarInsn(LLOAD, 5);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "setTime", "(J)V", false);
        mv.visitLabel(l0);
        mv.visitLineNumber(52, l0);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitMethodInsn(INVOKESTATIC, "ec/edu/utpl/datalab/codelogs/spyder/core/util/JsonMapper", "writte", "(Ljava/lang/Object;)Ljava/lang/String;", false);
        mv.visitVarInsn(ASTORE, 7);
        Label l20 = new Label();
        mv.visitLabel(l20);
        mv.visitLineNumber(53, l20);
        mv.visitLdcInsn("run");
        mv.visitVarInsn(ALOAD, 7);
        mv.visitMethodInsn(INVOKESTATIC, "ec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command", "createCommand", "(Ljava/lang/String;Ljava/lang/String;)Lec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command$CommandMake;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command$CommandMake", "build", "()Lec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command;", false);
        mv.visitVarInsn(ASTORE, 8);
        Label l21 = new Label();
        mv.visitLabel(l21);
        mv.visitLineNumber(54, l21);
        mv.visitFieldInsn(GETSTATIC, CLASS_NAME, "connectionProces", "Lec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess;");
        mv.visitVarInsn(ALOAD, 8);
        mv.visitMethodInsn(INVOKESTATIC, "ec/edu/utpl/datalab/codelogs/spyder/core/util/JsonMapper", "writte", "(Ljava/lang/Object;)Ljava/lang/String;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess", "send", "(Ljava/lang/String;)V", false);
        mv.visitLabel(l1);
        mv.visitLineNumber(57, l1);
        Label l22 = new Label();
        mv.visitJumpInsn(GOTO, l22);
        mv.visitLabel(l2);
        mv.visitLineNumber(55, l2);
        mv.visitFrame(Opcodes.F_FULL, 4, new Object[]{"ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", Opcodes.LONG, Opcodes.LONG, Opcodes.LONG}, 1, new Object[]{"java/io/IOException"});
        mv.visitVarInsn(ASTORE, 7);
        Label l23 = new Label();
        mv.visitLabel(l23);
        mv.visitLineNumber(56, l23);
        mv.visitVarInsn(ALOAD, 7);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/IOException", "printStackTrace", "()V", false);
        mv.visitLabel(l22);
        mv.visitLineNumber(58, l22);
        mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
        mv.visitFieldInsn(GETSTATIC, CLASS_NAME, "connectionProces", "Lec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess;");
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess", "disconnect", "()V", false);
        Label l24 = new Label();
        mv.visitLabel(l24);
        mv.visitLineNumber(59, l24);
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
        mv.visitInsn(DUP);
        mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
        mv.visitLdcInsn("Runtime segundos ");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
        mv.visitFieldInsn(GETSTATIC, "java/util/concurrent/TimeUnit", "MILLISECONDS", "Ljava/util/concurrent/TimeUnit;");
        mv.visitVarInsn(LLOAD, 5);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/concurrent/TimeUnit", "toSeconds", "(J)J", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(J)Ljava/lang/StringBuilder;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
        mv.visitInsn(ICONST_0);
        mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/String", "format", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
        Label l25 = new Label();
        mv.visitLabel(l25);
        mv.visitLineNumber(60, l25);
        Label l26 = new Label();
        mv.visitJumpInsn(GOTO, l26);
        mv.visitLabel(l5);
        mv.visitLineNumber(38, l5);
        mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{"java/lang/Exception"});
        mv.visitVarInsn(ASTORE, 7);
        Label l27 = new Label();
        mv.visitLabel(l27);
        mv.visitLineNumber(39, l27);
        mv.visitVarInsn(ALOAD, 7);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass", "()Ljava/lang/Class;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName", "()Ljava/lang/String;", false);
        mv.visitVarInsn(ASTORE, 8);
        Label l28 = new Label();
        mv.visitLabel(l28);
        mv.visitLineNumber(40, l28);
        mv.visitVarInsn(ALOAD, 7);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Exception", "getMessage", "()Ljava/lang/String;", false);
        mv.visitVarInsn(ASTORE, 9);
        Label l29 = new Label();
        mv.visitLabel(l29);
        mv.visitLineNumber(41, l29);
        mv.visitVarInsn(ALOAD, 7);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Exception", "getCause", "()Ljava/lang/Throwable;", false);
        Label l30 = new Label();
        mv.visitJumpInsn(IFNULL, l30);
        mv.visitVarInsn(ALOAD, 7);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Exception", "getCause", "()Ljava/lang/Throwable;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Throwable", "getLocalizedMessage", "()Ljava/lang/String;", false);
        Label l31 = new Label();
        mv.visitJumpInsn(GOTO, l31);
        mv.visitLabel(l30);
        mv.visitFrame(Opcodes.F_APPEND, 3, new Object[]{"java/lang/Exception", "java/lang/String", "java/lang/String"}, 0, null);
        mv.visitLdcInsn("unknow");
        mv.visitLabel(l31);
        mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[]{"java/lang/String"});
        mv.visitVarInsn(ASTORE, 10);
        Label l32 = new Label();
        mv.visitLabel(l32);
        mv.visitLineNumber(42, l32);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitFieldInsn(GETSTATIC, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/TYPE_INSTRUCTION_PACK", "HEART_BEAT_RUN_FAILED", "Lec/edu/utpl/datalab/codelogs/spyder/libs/transfers/TYPE_INSTRUCTION_PACK;");
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "setInstruction", "(Lec/edu/utpl/datalab/codelogs/spyder/libs/transfers/TYPE_INSTRUCTION_PACK;)V", false);
        Label l33 = new Label();
        mv.visitLabel(l33);
        mv.visitLineNumber(43, l33);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitVarInsn(ALOAD, 9);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "setMessageL1", "(Ljava/lang/String;)V", false);
        Label l34 = new Label();
        mv.visitLabel(l34);
        mv.visitLineNumber(44, l34);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitVarInsn(ALOAD, 10);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "setMessageL2", "(Ljava/lang/String;)V", false);
        Label l35 = new Label();
        mv.visitLabel(l35);
        mv.visitLineNumber(45, l35);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitVarInsn(ALOAD, 8);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "setMessageL3", "(Ljava/lang/String;)V", false);
        Label l36 = new Label();
        mv.visitLabel(l36);
        mv.visitLineNumber(46, l36);
        mv.visitTypeInsn(NEW, "java/lang/RuntimeException");
        mv.visitInsn(DUP);
        mv.visitVarInsn(ALOAD, 7);
        mv.visitMethodInsn(INVOKESPECIAL, "java/lang/RuntimeException", "<init>", "(Ljava/lang/Throwable;)V", false);
        mv.visitInsn(ATHROW);
        mv.visitLabel(l6);
        mv.visitLineNumber(48, l6);
        mv.visitFrame(Opcodes.F_FULL, 4, new Object[]{"ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", Opcodes.LONG, Opcodes.LONG, Opcodes.LONG}, 1, new Object[]{"java/lang/Throwable"});
        mv.visitVarInsn(ASTORE, 11);
        mv.visitLabel(l10);
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/System", "currentTimeMillis", "()J", false);
        mv.visitVarInsn(LSTORE, 3);
        Label l37 = new Label();
        mv.visitLabel(l37);
        mv.visitLineNumber(49, l37);
        mv.visitVarInsn(LLOAD, 3);
        mv.visitVarInsn(LLOAD, 1);
        mv.visitInsn(LSUB);
        mv.visitVarInsn(LSTORE, 5);
        Label l38 = new Label();
        mv.visitLabel(l38);
        mv.visitLineNumber(50, l38);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitVarInsn(LLOAD, 5);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", "setTime", "(J)V", false);
        mv.visitLabel(l7);
        mv.visitLineNumber(52, l7);
        mv.visitVarInsn(ALOAD, 0);
        mv.visitMethodInsn(INVOKESTATIC, "ec/edu/utpl/datalab/codelogs/spyder/core/util/JsonMapper", "writte", "(Ljava/lang/Object;)Ljava/lang/String;", false);
        mv.visitVarInsn(ASTORE, 12);
        Label l39 = new Label();
        mv.visitLabel(l39);
        mv.visitLineNumber(53, l39);
        mv.visitLdcInsn("run");
        mv.visitVarInsn(ALOAD, 12);
        mv.visitMethodInsn(INVOKESTATIC, "ec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command", "createCommand", "(Ljava/lang/String;Ljava/lang/String;)Lec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command$CommandMake;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command$CommandMake", "build", "()Lec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command;", false);
        mv.visitVarInsn(ASTORE, 13);
        Label l40 = new Label();
        mv.visitLabel(l40);
        mv.visitLineNumber(54, l40);
        mv.visitFieldInsn(GETSTATIC, CLASS_NAME, "connectionProces", "Lec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess;");
        mv.visitVarInsn(ALOAD, 13);
        mv.visitMethodInsn(INVOKESTATIC, "ec/edu/utpl/datalab/codelogs/spyder/core/util/JsonMapper", "writte", "(Ljava/lang/Object;)Ljava/lang/String;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess", "send", "(Ljava/lang/String;)V", false);
        mv.visitLabel(l8);
        mv.visitLineNumber(57, l8);
        Label l41 = new Label();
        mv.visitJumpInsn(GOTO, l41);
        mv.visitLabel(l9);
        mv.visitLineNumber(55, l9);
        mv.visitFrame(Opcodes.F_FULL, 9, new Object[]{"ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", Opcodes.LONG, Opcodes.LONG, Opcodes.LONG, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, "java/lang/Throwable"}, 1, new Object[]{"java/io/IOException"});
        mv.visitVarInsn(ASTORE, 12);
        Label l42 = new Label();
        mv.visitLabel(l42);
        mv.visitLineNumber(56, l42);
        mv.visitVarInsn(ALOAD, 12);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/IOException", "printStackTrace", "()V", false);
        mv.visitLabel(l41);
        mv.visitLineNumber(58, l41);
        mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
        mv.visitFieldInsn(GETSTATIC, CLASS_NAME, "connectionProces", "Lec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess;");
        mv.visitMethodInsn(INVOKEVIRTUAL, "ec/edu/utpl/datalab/codelogs/spyder/core/os/AbstractConnectionProcess", "disconnect", "()V", false);
        Label l43 = new Label();
        mv.visitLabel(l43);
        mv.visitLineNumber(59, l43);
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
        mv.visitInsn(DUP);
        mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V", false);
        mv.visitLdcInsn("Runtime segundos ");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(Ljava/lang/String;)Ljava/lang/StringBuilder;", false);
        mv.visitFieldInsn(GETSTATIC, "java/util/concurrent/TimeUnit", "MILLISECONDS", "Ljava/util/concurrent/TimeUnit;");
        mv.visitVarInsn(LLOAD, 5);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/concurrent/TimeUnit", "toSeconds", "(J)J", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(J)Ljava/lang/StringBuilder;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;", false);
        mv.visitInsn(ICONST_0);
        mv.visitTypeInsn(ANEWARRAY, "java/lang/Object");
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/String", "format", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", false);
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
        mv.visitVarInsn(ALOAD, 11);
        mv.visitInsn(ATHROW);
        mv.visitLabel(l26);
        mv.visitLineNumber(61, l26);
        mv.visitFrame(Opcodes.F_FULL, 4, new Object[]{"ec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack", Opcodes.LONG, Opcodes.LONG, Opcodes.LONG}, 0, new Object[]{});
        mv.visitInsn(RETURN);
        Label l44 = new Label();
        mv.visitLabel(l44);
        mv.visitLocalVariable("data", "Ljava/lang/String;", null, l20, l1, 7);
        mv.visitLocalVariable("command", "Lec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command;", null, l21, l1, 8);
        mv.visitLocalVariable("e", "Ljava/io/IOException;", null, l23, l22, 7);
        mv.visitLocalVariable("classname", "Ljava/lang/String;", null, l28, l6, 8);
        mv.visitLocalVariable("message", "Ljava/lang/String;", null, l29, l6, 9);
        mv.visitLocalVariable("lmessage", "Ljava/lang/String;", null, l32, l6, 10);
        mv.visitLocalVariable("ex", "Ljava/lang/Exception;", null, l27, l6, 7);
        mv.visitLocalVariable("data", "Ljava/lang/String;", null, l39, l8, 12);
        mv.visitLocalVariable("command", "Lec/edu/utpl/datalab/codelogs/spyder/libs/terminal/Command;", null, l40, l8, 13);
        mv.visitLocalVariable("e", "Ljava/io/IOException;", null, l42, l41, 12);
        mv.visitLocalVariable("blockCompileRun", "Lec/edu/utpl/datalab/codelogs/spyder/libs/transfers/HeartBeatBuildRunPack;", null, l12, l44, 0);
        mv.visitLocalVariable("start", "J", null, l15, l44, 1);
        mv.visitLocalVariable("end", "J", null, l16, l44, 3);
        mv.visitLocalVariable("delta", "J", null, l3, l44, 5);
        mv.visitMaxs(5, 14);
        mv.visitEnd();
    }


    /**
     * Modifica el punto de entrada del programa
     *
     * @param mv
     */
    public static void modificarMain(MethodNode mv) {
        mv.visitCode();
        Label l0 = new Label();
        mv.visitLabel(l0);
        mv.visitLineNumber(22, l0);
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitLdcInsn("          --------- CODELOGS UTPL ----------");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
        Label l1 = new Label();
        mv.visitLabel(l1);
        mv.visitLineNumber(23, l1);
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitLdcInsn("UTPL Plugin V1.0");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
        Label l2 = new Label();
        mv.visitLabel(l2);
        mv.visitLineNumber(24, l2);
        mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
        mv.visitLdcInsn("==========================================================");
        mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
        Label l3 = new Label();
        mv.visitLabel(l3);
        mv.visitLineNumber(25, l3);
        mv.visitMethodInsn(INVOKESTATIC, CLASS_NAME, PROGRAMM_WITH_EXC, "()V", false);
        Label l4 = new Label();
        mv.visitLabel(l4);
        mv.visitLineNumber(26, l4);
        mv.visitInsn(RETURN);
        Label l5 = new Label();
        mv.visitLabel(l5);
        mv.visitLocalVariable("args", "[Ljava/lang/String;", null, l0, l5, 0);
        mv.visitMaxs(2, 1);
        mv.visitEnd();
    }

}
