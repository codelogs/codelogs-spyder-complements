/*
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin;

import org.apache.maven.execution.BuildFailure;
import org.apache.maven.execution.BuildSuccess;
import org.apache.maven.execution.BuildSummary;

import java.net.URL;

import static com.google.common.base.Preconditions.checkNotNull;

public enum Status {

    SUCCESS("/icons/dialog-clean.png", "Success"),
    FAILURE("/icons/dialog-error-5.png", "Failure"),
    SKIPPED(null, "Skipped");

    private final String icon;
    private final String message;

    Status(String icon, String message) {
        this.icon = icon;
        this.message = checkNotNull(message);
    }

    public String message() {
        return message;
    }

    public static Status of(BuildSummary summary) {
        if (summary == null) {
            return SKIPPED;
        } else if (summary instanceof BuildSuccess) {
            return SUCCESS;
        } else if (summary instanceof BuildFailure) {
            return FAILURE;
        }
        throw new IllegalArgumentException(String.format("Summary status type [%s] is not handle.", summary.getClass().getName()));
    }

    public URL url() {
        return getClass().getResource(icon);
    }
}
