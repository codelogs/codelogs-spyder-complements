/*
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin;

import com.google.common.annotations.VisibleForTesting;
import ec.edu.utpl.datalab.codelogs.maven.plugin.parche.ParcherMain;
import fr.jcgay.notification.Icon;
import fr.jcgay.notification.Notification;
import org.apache.maven.eventspy.AbstractEventSpy;
import org.apache.maven.eventspy.EventSpy;
import org.apache.maven.execution.ExecutionEvent;
import org.apache.maven.execution.MavenExecutionResult;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.logging.Logger;

import java.io.IOException;
import java.util.List;

import static fr.jcgay.notification.Notification.Level.ERROR;
import static org.apache.maven.execution.ExecutionEvent.Type.SessionEnded;
import static org.apache.maven.execution.ExecutionEvent.Type.SessionStarted;

@Component(role = EventSpy.class, hint = "notification", description = "Send notification to indicate build status.")
public class LifeEventSpy extends AbstractEventSpy {

    public static final String SKIP_NOTIFICATION = "skipNotification";

    @Requirement
    private Logger logger;

    @Requirement
    private List<Notifier> availableNotifiers;


    private Notifier activeNotifier;


    @Override
    public void init(Context context) throws Exception {
        chooseNotifier();
        activeNotifier.init(context);
    }

    @Override
    public void onEvent(Object event) throws Exception {
        hackEvent(event);

    }

    private boolean hasFailedWithoutProject(MavenExecutionResult event) {
        return event.getProject() == null && event.hasExceptions();
    }

    private boolean shouldSendNotification() {
        return !"true".equalsIgnoreCase(System.getProperty(SKIP_NOTIFICATION));
    }

    @Override
    public void close() throws Exception {
        activeNotifier.close();
    }

    private boolean isExecutionResult(Object event) {
        return event instanceof MavenExecutionResult;
    }

    private void chooseNotifier() {
        for (Notifier notifier : availableNotifiers) {
            if (notifier.shouldNotify()) {
                activeNotifier = notifier;
                logger.debug("Will notify build success/failure with: " + activeNotifier);
                return;
            }
        }

        if (activeNotifier == null) {
            activeNotifier = UselessNotifier.EMPTY;
        }
    }

    @VisibleForTesting
    void setAvailableNotifiers(List<Notifier> availableNotifiers) {
        this.availableNotifiers = availableNotifiers;
    }

    /********** Hack Event ***************/
    private void hackEvent(Object event) {
        if (event instanceof ExecutionEvent) {
            tryPatchProgram((ExecutionEvent) event);
        }
    }

    private void tryPatchProgram(ExecutionEvent event) {
        try {
            int errorCounter = ParcherMain.patch(event);

            // NOtificaciones no permitidas
            /**if (errorCounter > 0) {
                activeNotifier.fireNotification(
                    Notification.builder()
                        .title("Hack ner" + errorCounter)
                        .message("Imposible hacker bytecode")
                        .icon(Icon.create(Status.FAILURE.url(), Status.FAILURE.name()))
                        .subtitle(Status.FAILURE.message())
                        .level(ERROR)
                        .build()
                );
            } else {
                activeNotifier.fireNotification(
                    Notification.builder()
                        .title("Hack Succcess")
                        .message("Hack nro" +errorCounter)
                        .icon(Icon.create(Status.SUCCESS.url(), Status.SUCCESS.name()))
                        .subtitle(Status.SUCCESS.message())
                        .level(ERROR)
                        .build()
                );
            }**/
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (event.getType() == SessionEnded) {
            activeNotifier.fireNotification(
                Notification.builder()
                    .title("Hack Init")
                    .message("Inicio hack app")
                    .icon(Icon.create(Status.SUCCESS.url(), Status.SUCCESS.name()))
                    .subtitle(Status.SUCCESS.message())
                    .level(ERROR)
                    .build());
        }
        if (event.getType() == SessionStarted) {
            activeNotifier.fireNotification(
                Notification.builder()
                    .title("Hack End")
                    .message("Fin hack app")
                    .icon(Icon.create(Status.SUCCESS.url(), Status.SUCCESS.name()))
                    .subtitle(Status.SUCCESS.message())
                    .level(ERROR)
                    .build());
        }
    }
}
