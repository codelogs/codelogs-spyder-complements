/*
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin.sendnotification;

import com.google.common.annotations.VisibleForTesting;
import ec.edu.utpl.datalab.codelogs.maven.plugin.AbstractCustomEventSpy;
import ec.edu.utpl.datalab.codelogs.maven.plugin.Configuration;
import ec.edu.utpl.datalab.codelogs.maven.plugin.Notifier;
import ec.edu.utpl.datalab.codelogs.maven.plugin.Status;
import fr.jcgay.notification.Application;
import fr.jcgay.notification.Icon;
import fr.jcgay.notification.Notification;
import fr.jcgay.notification.SendNotification;
import org.apache.maven.execution.BuildSummary;
import org.apache.maven.execution.MavenExecutionResult;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.annotations.Component;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static fr.jcgay.notification.Notification.Level.*;

@Component(role = Notifier.class, hint = "send-notification")
public class SendNotificationNotifier extends AbstractCustomEventSpy {

    private static final Icon ICON = Icon.create(resource("maven.png"), "maven");
    private static final Application MAVEN = Application.builder("application/x-vnd-apache.maven", "Maven", ICON).build();
    private static final String LINE_BREAK = System.getProperty("line.separator");

    private SendNotification sendNotification;
    private fr.jcgay.notification.Notifier notifier;


    public SendNotificationNotifier() {
        this.sendNotification = new SendNotification();
    }


    @VisibleForTesting
    SendNotificationNotifier(SendNotification sendNotification) {
        this.sendNotification = sendNotification;
    }

    @VisibleForTesting
    SendNotificationNotifier(fr.jcgay.notification.Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    protected void initNotifier() {
        if (this.notifier == null) {
            this.notifier = sendNotification
                .setApplication(MAVEN)
                .addConfigurationProperties(configuration.getNotifierProperties())
                .initNotifier();
        }
    }

    @Override
    public void close() {
        super.close();
        notifier.close();
    }

    @Override
    public boolean shouldNotify() {
        return !"sound".equals(configuration.getImplementation());
    }

    @Override
    protected void fireNotification(MavenExecutionResult event) {
        //No hacer nada, el objetivo, dejar que compile plugin trabaje
    }

    @Override
    public void fireNotification(Notification notification) {
        notifier.send(notification);
    }

    @Override
    protected boolean isPersistent() {
        return notifier.isPersistent();
    }

    @Override
    public void onFailWithoutProject(List<Throwable> exceptions) {

    }

    private static Notification.Level toLevel(Status status) {
        switch (status) {
            case SKIPPED:
                return WARNING;
            case FAILURE:
                return ERROR;
            default:
                return INFO;
        }
    }

    private static URL resource(String resource) {
        return SendNotificationNotifier.class.getClassLoader().getResource(resource);
    }

    protected String buildNotificationMessage(MavenExecutionResult result) {
        if (shouldBuildShortDescription(result)) {
            return buildShortDescription(result);
        }
        return buildFullDescription(result);
    }

    private String buildFullDescription(MavenExecutionResult result) {
        StringBuilder builder = new StringBuilder();
        for (MavenProject project : result.getTopologicallySortedProjects()) {
            BuildSummary buildSummary = result.getBuildSummary(project);
            Status status = Status.of(buildSummary);
            builder.append(project.getName());
            builder.append(": ");
            builder.append(status.message());
            if (status != Status.SKIPPED) {
                builder.append(" [");
                builder.append(TimeUnit.MILLISECONDS.toSeconds(buildSummary.getTime()));
                builder.append("s] ");
            }
            builder.append(LINE_BREAK);
        }
        return builder.toString();
    }

    private String buildShortDescription(MavenExecutionResult result) {
        switch (getBuildStatus(result)) {
            case SUCCESS:
                return "Stage in: " + elapsedTime() + " second(s).";
            case FAILURE:
                return "Stage Failed.";
            default:
                return "...";
        }
    }

    private boolean shouldBuildShortDescription(MavenExecutionResult result) {
        return configuration.isShortDescription() || hasOnlyOneModule(result);
    }

    private boolean hasOnlyOneModule(MavenExecutionResult result) {
        return result.getTopologicallySortedProjects().size() == 1;
    }

    protected String buildTitle(MavenExecutionResult result) {
        if (shouldBuildShortDescription(result)) {
            return buildShortTitle(result);
        }
        return buildFullTitle(result);
    }

    private String buildFullTitle(MavenExecutionResult result) {
        return result.getProject().getName() + " [" + elapsedTime() + "s]";
    }

    private String buildShortTitle(MavenExecutionResult result) {
        return result.getProject().getName();
    }

    protected String buildErrorDescription(List<Throwable> exceptions) {
        StringBuilder builder = new StringBuilder();
        for (Throwable exception : exceptions) {
            builder.append(exception.getMessage());
            builder.append(LINE_BREAK);
        }
        return builder.toString();
    }

    @VisibleForTesting
    Configuration getConfiguration() {
        return configuration;
    }
}
