/*
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin.parche;

import ec.edu.utpl.datalab.codelogs.hack.bytecode.ITransformer;
import ec.edu.utpl.datalab.codelogs.hack.bytecode.ResultTransform;
import ec.edu.utpl.datalab.codelogs.hack.bytecode.Transformer;
import ec.edu.utpl.datalab.codelogs.spyder.libs.common.FileUtils;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.FilterSet;
import ec.edu.utpl.datalab.codelogs.spyder.libs.filesystem.search.SearchFileSystem;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.VFS;
import org.apache.maven.execution.ExecutionEvent;

import java.io.IOException;
import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class ParcherMain {
    public static int patch(ExecutionEvent event) throws IOException {

        int errorCounter = 0;
        System.out.println("Parche automatico compelemnto codelogs" + event.getMojoExecution().getGoal());


        if (Objects.isNull(event.getProject())) {
            return 0;
        }
        String project = event.getProject().getBuild().getOutputDirectory();
       // System.out.println(project);
        String phase = (event.getMojoExecution().getLifecyclePhase()==null)?"unknow":event.getMojoExecution().getLifecyclePhase();
       // System.out.println(phase);
        String goal = event.getMojoExecution().getGoal();
        //System.out.println(goal);

        // si la fase fue compilacion, patch main method
        if (phase.equals("compile") || goal.equals("exec")) {
           // System.out.println("Configurando codigo del estudiante ......");
            SearchFileSystem searchFileSystem = SearchFileSystem.newSearch(15)
                .withFilter(FilterSet.filterByExtension(".class"))
                .withFilter(FilterSet.filterOnlyFiles())
                .build();

            FileObject fileObject = VFS.getManager().resolveFile(project);
            FileObject[] result = searchFileSystem.executeFinder(fileObject);
            for (FileObject object : result) {
               // System.out.println("Codelogs gost " + object.getName().getBaseName());
               // System.out.println("------- " + object.getName().getPath());
                ResultTransform resultTransform = null;
                try {
                 //   System.out.println("Transformando ->" + object.getName().getFriendlyURI());
                    ITransformer transformer = new Transformer(object.getName().getFriendlyURI());
                    resultTransform = transformer.transform(object.getContent().getInputStream());
                    if (!resultTransform.isOriginal()) {
                        FileUtils.writeToFile(resultTransform.getResult(), object.getName().getPathDecoded());
                    }
                } catch (Exception e) {
                    errorCounter++;
                }
            }
        }

        return errorCounter;
        //FileObject fileObject = VFS.getManager().resolveFile("/home/rfcardenas/Escritorio/app");
    }

}
