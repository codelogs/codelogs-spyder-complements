/*
 * The MIT License (MIT)
 * Copyright (c) 2016 rfcardenas
 * Module : codelogs-maven-plugin
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package ec.edu.utpl.datalab.codelogs.maven.plugin;

import fr.jcgay.notification.Notification;
import org.apache.maven.eventspy.EventSpy;
import org.apache.maven.execution.MavenExecutionResult;

import java.util.List;

public interface Notifier {

    /**
     * Indicate if the notifier is an implementation for the provided parameter.
     *
     * @return {@code true} if the notifier should be used to notify the build status.
     */
    boolean shouldNotify();

    /**
     * Initializes the spy.
     *
     * @param context The event spy context, never {@code null}.
     */
    void init(EventSpy.Context context);


    /**
     * Notifies the notifier of build result.
     */
    void onEvent(MavenExecutionResult event);

    /**
     * Notifies the notifier of Maven's termination, allowing it to free any resources allocated by it.
     */
    void close();

    /**
     * Notifies the notifier of a build ends with error without information about the build. <br />
     * This is for example, a malformed {@code pom.xml} which breaks everything.
     * @param exceptions errors that happened
     */
    void onFailWithoutProject(List<Throwable> exceptions);

    void fireNotification(Notification notification);

}
